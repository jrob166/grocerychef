# README #

GroceryChef is an Android application that allows a user to create recipes and grocery lists.

Things to be implemented in the future:
* Ability to add recipes to grocery list

![Home Screen.JPG](https://bitbucket.org/repo/j8zo9x/images/1874845270-Home%20Screen.JPG)
![2nd Screen.JPG](https://bitbucket.org/repo/j8zo9x/images/3088640010-2nd%20Screen.JPG)
![Recipe Screen.JPG](https://bitbucket.org/repo/j8zo9x/images/703314337-Recipe%20Screen.JPG)
![3rd Screen.JPG](https://bitbucket.org/repo/j8zo9x/images/4125163867-3rd%20Screen.JPG)