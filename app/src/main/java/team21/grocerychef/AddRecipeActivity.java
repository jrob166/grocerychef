package team21.grocerychef;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AddRecipeActivity extends AppCompatActivity implements MyCallBack {

    public static final int EDIT_ITEM = 0;
    public static final int ADD_ITEM = 1;
    private static List<FoodItem> ingredientList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextView recipeName;
    private GroceryAdapter adapter;
    private int position;
    Recipe recipe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipe);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new GroceryAdapter(ingredientList);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addItemDecoration(new ItemDivider(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);

        recipeName = (TextView) findViewById(R.id.recipe_name);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            position = extras.getInt("Position");
            recipe = (Recipe) getIntent().getSerializableExtra("item");
            recipeName.setText(recipe.getName());
        }

        if(ingredientList.size()== 0){
            Toast.makeText(AddRecipeActivity.this, "Click the add button to add ingredients to your recipe", Toast.LENGTH_LONG).show();
        }

        final ImageView addItem = (ImageView) findViewById(R.id.add);
        addItem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                goToAddItem(view);
            }
        });

        final Button saveButton = (Button)findViewById(R.id.save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveValues(v);
                setRecipeResult();
            }
        });

    }

    private void setRecipeResult(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("item", recipe);
        returnIntent.putExtra("position", position);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    public void saveValues(View view){
        String newName = recipeName.getText().toString();
        recipe = new Recipe(newName);
    }

    public void goToAddItem(View view){
        Intent intent = new Intent(this, AddItemActivity.class);
        startActivityForResult(intent, ADD_ITEM);
    }

    @Override
    public void onHandleSelection(int position, Object item) {
        Intent editItemActivity = new Intent(this, EditItemActivity.class);
        editItemActivity.putExtra("item", ingredientList.get(position));
        editItemActivity.putExtra("Position", position);
        startActivityForResult(editItemActivity, EDIT_ITEM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == EDIT_ITEM) {
                int position = data.getIntExtra("position", 0);
                FoodItem updatedItem = (FoodItem) data.getSerializableExtra("item");
                ingredientList.set(position, updatedItem);
                adapter.notifyDataSetChanged();
            }
            if(requestCode == ADD_ITEM){
                FoodItem newItem = (FoodItem) data.getSerializableExtra("item");
                ingredientList.add(newItem);
                adapter.notifyDataSetChanged();
            }
        }


    }
}
