package team21.grocerychef;

import java.io.Serializable;

public class FoodItem implements Serializable{
    //this class is for any food item that a user may want to put on the grocery list that is not an ingredient

    private String name;
    private double quantity;
    private String unit;

    public FoodItem(String name) {

    }

    public FoodItem(String name, double quantity){
        //this is for when you want to specify a quantity. Again you probably need to implement something
        //for units
    }

    public FoodItem(String name, double quantity, String unit){
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
    }


    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
