package team21.grocerychef;

/**
 * Created by Jackie on 4/21/2016.
 */
public interface MyCallBack <T> {
       public void onHandleSelection(int position, T item);
}
