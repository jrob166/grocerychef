package team21.grocerychef;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeAdapter.MyViewHolder> {

    private List<Recipe> recipeList;
    private MyCallBack callBack;
    Recipe recipe;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, quantity, unit;
        public Context context;

        public MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            callBack = (MyCallBack) context;
            name = (TextView) view.findViewById(R.id.recipe_name);
        }
    }

    public RecipeAdapter(List<Recipe> recipeList) {
        this.recipeList = recipeList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recipe_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        recipe = recipeList.get(position);
        holder.name.setText(recipe.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onHandleSelection(position, recipe);
            }
        });
    }

    @Override
    public int getItemCount() {
        return recipeList.size();
    }
}

