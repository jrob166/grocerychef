package team21.grocerychef;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class AddItemActivity extends AppCompatActivity {
    private TextView itemName;
    private TextView itemQuantity;
    private TextView itemUnit;
    private FoodItem foodItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            itemName = (TextView) findViewById(R.id.item_name);
            itemQuantity = (TextView) findViewById(R.id.item_quantity);
            itemUnit = (TextView) findViewById(R.id.item_unit);

        final Button save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveValues(v);
                setFoodItemResult();
            }
        });
    }

    private void setFoodItemResult(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("item", foodItem);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    public void saveValues(View view){
        String newName = itemName.getText().toString();
        double newQuantity = Double.valueOf(itemQuantity.getText().toString());
        String newUnit = itemUnit.getText().toString();
        foodItem = new FoodItem(newName,newQuantity,newUnit);
    }

}
