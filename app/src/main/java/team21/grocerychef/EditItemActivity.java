package team21.grocerychef;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class EditItemActivity extends AppCompatActivity {

    private TextView itemName;
    private TextView itemQuantity;
    private TextView itemUnit;
    private FoodItem foodItem;
    private int positon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_item);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            positon = extras.getInt("Position");
            foodItem = (FoodItem) getIntent().getSerializableExtra("item");
            itemName = (TextView) findViewById(R.id.item_name);
            itemName.setText(foodItem.getName());
            itemQuantity = (TextView) findViewById(R.id.item_quantity);
            itemQuantity.setText(String.valueOf(foodItem.getQuantity()));
            itemUnit = (TextView) findViewById(R.id.item_unit);
            itemUnit.setText(foodItem.getUnit());
        }

        final Button save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveValues(v);
                setFoodItemResult();
            }
        });
    }

    private void setFoodItemResult(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("item", foodItem);
        returnIntent.putExtra("position", positon);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    public void saveValues(View view){
        String newName = itemName.getText().toString();
        foodItem.setName(newName);
        double newQuantity = Double.valueOf(itemQuantity.getText().toString());
        foodItem.setQuantity(newQuantity);
        String newUnit = itemUnit.getText().toString();
        foodItem.setUnit(newUnit);
    }

}
