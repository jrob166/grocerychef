package team21.grocerychef;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import team21.grocerychef.sqlite.model.*;

public class RecipeListActivity extends AppCompatActivity implements MyCallBack  {

    private static ArrayList<Recipe> recipeList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RecipeAdapter adapter;
    public static final int EDIT_ITEM = 0;
    public static final int ADD_ITEM = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new RecipeAdapter(recipeList);
        setContentView(R.layout.activity_recipe_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setUpRecyclerView();

        setAddListener();

        if(recipeList.size() == 0){
            Toast.makeText(RecipeListActivity.this, "Click the add button to add recipes to your list", Toast.LENGTH_LONG).show();
        }

    }

    protected void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addItemDecoration(new ItemDivider(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);
    }

    protected void setAddListener(){
        final ImageView addItem = (ImageView) findViewById(R.id.add);
        addItem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                goToAddRecipe(view);
            }
        });
    }

    public void goToAddRecipe(View view){
        Intent intent = new Intent(this, AddRecipeActivity.class);
        startActivityForResult(intent,ADD_ITEM);
    }

    public void onHandleSelection(int position, Object item) {
        Intent addRecipeActivity = new Intent(this, AddRecipeActivity.class);
        addRecipeActivity.putExtra("item", recipeList.get(position));
        addRecipeActivity.putExtra("Position", position);
        startActivityForResult(addRecipeActivity, EDIT_ITEM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == EDIT_ITEM) {
                int position = data.getIntExtra("position", 0);
                Recipe updatedItem = (Recipe) data.getSerializableExtra("item");
                recipeList.set(position, updatedItem);
                adapter.notifyDataSetChanged();
            }
            if(requestCode == ADD_ITEM){
                Recipe newItem = (Recipe) data.getSerializableExtra("item");
                recipeList.add(newItem);
                adapter.notifyDataSetChanged();
            }
        }


    }
}
