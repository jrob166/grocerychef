package team21.grocerychef;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class EditRecipeActivity extends AppCompatActivity {


    private TextView recipeName;
    private Recipe recipe;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_recipe);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            position = extras.getInt("Position");
            recipe = (Recipe) getIntent().getSerializableExtra("item");
            recipeName = (TextView) findViewById(R.id.recipe_name);
            recipeName.setText(recipe.getName());
        }

        final Button save = (Button) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveValues(v);
                setFoodItemResult();
            }
        });
    }

    private void setFoodItemResult(){
        Intent returnIntent = new Intent();
        returnIntent.putExtra("item", recipe);
        returnIntent.putExtra("position", position);
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }

    public void saveValues(View view){
        String newName = recipeName.getText().toString();
        recipe.setName(newName);
    }


}
