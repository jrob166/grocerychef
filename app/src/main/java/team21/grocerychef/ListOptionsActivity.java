package team21.grocerychef;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class ListOptionsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_options);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //creates grocery list button
        final Button groceryListButton = (Button) findViewById(R.id.grocery_list_button);
        groceryListButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                goToGroceryList(view);
            }
        });

        final Button recipeListButton = (Button) findViewById(R.id.recipes_Button);
        recipeListButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToRecipeList(v);
            }
        });
    }

    // goes to a page with displays the user's grocery list
    public void goToGroceryList(View view){
        Intent intent = new Intent(this, GroceryListActivity.class);
        startActivity(intent);
    }

    public void goToRecipeList(View view){
        Intent intent = new Intent(this, RecipeListActivity.class);
        startActivity(intent);
    }

}
