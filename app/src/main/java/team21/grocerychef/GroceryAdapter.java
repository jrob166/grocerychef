package team21.grocerychef;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class GroceryAdapter extends RecyclerView.Adapter<GroceryAdapter.MyViewHolder> {

    private List<FoodItem> groceryList;
    private MyCallBack callBack;
    FoodItem foodItem;
   private  int index;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, quantity, unit;
        public Context context;

        public MyViewHolder(View view) {
            super(view);
            context = view.getContext();
            callBack = (MyCallBack) context;
            name = (TextView) view.findViewById(R.id.name);
            quantity = (TextView) view.findViewById(R.id.quantity);
            unit = (TextView) view.findViewById(R.id.unit);
        }
    }

    public GroceryAdapter(List<FoodItem> groceryList) {
        this.groceryList = groceryList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grocery_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        foodItem = groceryList.get(position);
        index = holder.getAdapterPosition();
        holder.name.setText(foodItem.getName());
        holder.quantity.setText(String.valueOf(foodItem.getQuantity()));
        holder.unit.setText(foodItem.getUnit());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.onHandleSelection(position, foodItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return groceryList.size();
    }


}
