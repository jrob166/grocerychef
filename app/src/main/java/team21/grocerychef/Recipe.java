package team21.grocerychef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Recipe implements Serializable{
    private String name;
    private ArrayList<FoodItem> ingredients = new ArrayList<>();

    public Recipe(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<FoodItem> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<FoodItem> ingredients) {
        this.ingredients = ingredients;
    }

    public void addIngredient(FoodItem foodItem){
        ingredients.add(foodItem);
    }
}
