package team21.grocerychef.sqlite.model;

public class FoodItem {
    private int itemId;
    private String name;
    private double quantity;
    private String unit;

    /*Constructors */

    public FoodItem(int id, String name){
        itemId = id;
        this.name = name;
    }

    public FoodItem(int id, String name, double quantity){
        itemId = id;
        this.name =name;
        this.quantity = quantity;
    }

    public FoodItem(int id, String name, double quantity, String unit){
        itemId = id;
        this.name = name;
        this.unit = unit;
    }




    /* Getters */
    public int getItemId() {
        return itemId;
    }

    public String getName() {
        return name;
    }

    public double getQuantity() {
        return quantity;
    }

    public String getUnit() {
        return unit;
    }


    /* Setters */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
