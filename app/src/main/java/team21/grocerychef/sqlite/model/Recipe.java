package team21.grocerychef.sqlite.model;

public class Recipe {
    private int recipeId;
    private String name;

    /*Constructors*/
    public Recipe(int id, String name){
        recipeId = id;
        this.name = name;
    }

    /* Getters */
    public int getRecipeId() {
        return recipeId;
    }

    public String getName() {
        return name;
    }


    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    public void setName(String name) {
        this.name = name;
    }
}
