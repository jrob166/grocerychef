//Jackie made a comment
package team21.grocerychef.sqlite.helper;
// my name is nina
// my name is also khadija
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.inputmethodservice.KeyboardView;

import team21.grocerychef.sqlite.model.FoodItem;
import team21.grocerychef.sqlite.model.Recipe;

public class DatabaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "groceryChef";

    // Table Names
    private static final String TABLE_FOOD_ITEMS = "food_items";
    private static final String TABLE_RECIPES = "recipe";
    private static final String TABLE_GROCERY_LIST = "grocery_list";

    // Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_RECIPE_ID = "recipe_id";

    // Food Item Table - column names
    private static final String KEY_QUANTITY = "quantity";
    private static final String KEY_UNIT = "unit";

    // Grocery List Table - column names
    private static final String KEY_FOOD_ITEM_ID = "food_item_id";


    // Table Create Statements
    // Food Item table create statement
    private static final String CREATE_TABLE_FOOD_ITEMS = "CREATE TABLE "
            + TABLE_FOOD_ITEMS + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME
            + " TEXT," + KEY_QUANTITY + " REAL," + KEY_UNIT
            + " TEXT" + ")";

    // Recipe table create statement
    private static final String CREATE_TABLE_RECIPE = "CREATE TABLE " + TABLE_RECIPES
            + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT," + ")";

    // todo_tag table create statement
    private static final String CREATE_TABLE_GROCERY_LIST = "CREATE TABLE "
            + TABLE_GROCERY_LIST + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_FOOD_ITEM_ID + " INTEGER," + KEY_RECIPE_ID + " INTEGER,"
            + ")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_FOOD_ITEMS);
        db.execSQL(CREATE_TABLE_RECIPE);
        db.execSQL(CREATE_TABLE_GROCERY_LIST);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FOOD_ITEMS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECIPES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GROCERY_LIST);
        onCreate(db);
    }

    public long createFoodItem(FoodItem foodItem, long[] food_item_ids){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, foodItem.getName());
        values.put(KEY_QUANTITY, foodItem.getQuantity());
        values.put(KEY_UNIT, foodItem.getUnit());

        long food_item_id = db.insert(TABLE_FOOD_ITEMS, null, values);

        return food_item_id;
    }

    public long createRecipe(Recipe recipe, long[] recipe_ids){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, recipe.getName());

        long recipe_id = db.insert(TABLE_RECIPES, null, values);

        return recipe_id;
    }

    public int updateRecipe(Recipe recipe) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, recipe.getName());

        // updating row
        return db.update(TABLE_RECIPES, values, KEY_ID + " = ?",
                new String[] { String.valueOf(recipe.getRecipeId()) });
    }
    public int updateFoodItem(FoodItem foodItem) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, foodItem.getName());
        values.put(KEY_QUANTITY,foodItem.getQuantity());
        values.put(KEY_UNIT, foodItem.getUnit());

        // updating row
        return db.update(TABLE_FOOD_ITEMS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(foodItem.getItemId()) });
    }

    /*
 * Deleting a todo
 */
    public void deleteRecipes(long recipe_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RECIPES, KEY_ID + " = ?",
                new String[] { String.valueOf(recipe_id) });
    }

    public void deleteFoodItem(long foodItem) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_FOOD_ITEMS, KEY_ID + " = ?",
                new String[] { String.valueOf(foodItem) });
    }


}
