package team21.grocerychef;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

public class GroceryListActivity extends AppCompatActivity implements MyCallBack {

    private static List<FoodItem> groceryList = new ArrayList<>();
    private RecyclerView recyclerView;
    private GroceryAdapter adapter;
    public static final int EDIT_ITEM = 0;
    public static final int ADD_ITEM = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter = new GroceryAdapter(groceryList);
        setContentView(R.layout.activity_grocery_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setUpRecyclerView();

        setAddListener();

        if(groceryList.size() == 0){
            Toast.makeText(GroceryListActivity.this, "Click the add button to add items to your list", Toast.LENGTH_LONG).show();
        }
    }

    protected void setAddListener() {
        final ImageView addItem = (ImageView) findViewById(R.id.add);
        addItem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                goToAddItem(view);
            }
        });
    }

    protected void setUpRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        recyclerView.addItemDecoration(new ItemDivider(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);
    }

    public void goToAddItem(View view){
        Intent intent = new Intent(this, AddItemActivity.class);
        startActivityForResult(intent,ADD_ITEM);
    }


    @Override
    public void onHandleSelection(int position, Object item) {
        Intent editItemActivity = new Intent(this, EditItemActivity.class);
        editItemActivity.putExtra("item",groceryList.get(position));
        editItemActivity.putExtra("Position", position);
        startActivityForResult(editItemActivity, EDIT_ITEM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            if(requestCode == EDIT_ITEM) {
                int position = data.getIntExtra("position", 0);
                FoodItem updatedItem = (FoodItem) data.getSerializableExtra("item");
                groceryList.set(position, updatedItem);
                adapter.notifyDataSetChanged();
            }
            if(requestCode == ADD_ITEM){
                FoodItem newItem = (FoodItem) data.getSerializableExtra("item");
                groceryList.add(newItem);
                adapter.notifyDataSetChanged();
            }
        }


    }
}
